FROM python:3

ENV PYTHONUBUFFERED 1

WORKDIR /code
COPY . /code/

EXPOSE 8000

VOLUME [ "/code/db" ]

RUN pip install -r requirements.txt
CMD sh init.sh && python3 manage.py runserver 0.0.0.0:8000